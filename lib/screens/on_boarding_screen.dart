import 'package:flutter/material.dart';
import 'package:flutter_assignment/screens/login/continue_with_email_screen.dart';
import 'package:flutter_assignment/screens/signup/signup_screen.dart';
import 'package:flutter_assignment/widgets/rounded_button.dart';

class OnBoardingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Icon(
                Icons.flag,
                size: 80.0,
                color: Color(0xFF50BE91),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                'Eveidence based data approach.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Inter',
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                'Autism, or autism spectrum disorder (ASD), refers to a broad range of conditions characterized by challenges with social skills.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Inter',
                  fontSize: 18.0,
                ),
              ),
            ),
            const SizedBox(height: 20.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 5.0,
                  backgroundColor: Colors.grey,
                ),
                const SizedBox(width: 5.0),
                CircleAvatar(
                  radius: 8.0,
                  backgroundColor: Colors.black,
                ),
                const SizedBox(width: 5.0),
                CircleAvatar(
                  radius: 5.0,
                  backgroundColor: Colors.grey,
                ),
              ],
            ),
            Spacer(),
            RoundedButton(
              backgroundColor: const Color(0xFF50BE91),
              textColor: Colors.white,
              text: 'Login',
              onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => ContinueWithEmailScreen())),
            ),
            const SizedBox(height: 20.0),
            RoundedButton(
              backgroundColor: Colors.white,
              textColor: const Color(0xFF50BE91),
              text: 'Sign Up',
              onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => SignupScreen())),
              border: Border.all(
                color: Colors.grey.withOpacity(0.3),
                width: 2.0,
              ),
            ),
            const SizedBox(height: 30.0),
          ],
        ),
      ),
    );
  }
}
