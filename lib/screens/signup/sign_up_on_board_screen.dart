import 'package:flutter/material.dart';
import 'package:flutter_assignment/widgets/rounded_button.dart';

final _scaffoldKey = GlobalKey<ScaffoldState>();

class SignUpOnBoardScreen extends StatefulWidget {
  @override
  _SignUpOnBoardScreenState createState() => _SignUpOnBoardScreenState();
}

class _SignUpOnBoardScreenState extends State<SignUpOnBoardScreen> {
  @override
  Widget build(BuildContext context) {
    final _pageController = PageController(initialPage: 0);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text('Sign up'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: _pageController,
          children: [
            NamePage(
              onNext: () {
                _pageController.nextPage(duration: const Duration(milliseconds: 300), curve: Curves.ease);
              },
            ),
            EmailAddressPage(
              onNext: () {
                _pageController.nextPage(duration: const Duration(milliseconds: 300), curve: Curves.ease);
              },
            ),
            PasswordPage(
              onNext: () {},
            ),
          ],
        ),
      ),
    );
  }
}

class NamePage extends StatelessWidget {
  final VoidCallback onNext;

  const NamePage({Key key, @required this.onNext}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Whats your name?',
              style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('1/3'),
          ],
        ),
        const SizedBox(
          height: 20.0,
        ),
        Text('Your full name'),
        const SizedBox(height: 10.0),
        TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
          ),
        ),
        const SizedBox(height: 20.0),
        Align(
          alignment: Alignment.centerRight,
          child: ElevatedButton(
            onPressed: () => onNext(),
            style: ButtonStyle(
              shape: MaterialStateProperty.all(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
              backgroundColor: MaterialStateProperty.all(Color(0xFF50BE91)),
              padding: MaterialStateProperty.all(EdgeInsets.symmetric(horizontal: 30.0)),
            ),
            child: Text('Next'),
          ),
        ),
      ],
    );
  }
}

class EmailAddressPage extends StatelessWidget {
  final VoidCallback onNext;

  const EmailAddressPage({Key key, @required this.onNext}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Whats your\nemail address?',
              style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('2/3'),
          ],
        ),
        const SizedBox(
          height: 20.0,
        ),
        Text('Email address'),
        const SizedBox(height: 10.0),
        TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
          ),
        ),
        const SizedBox(height: 20.0),
        Align(
          alignment: Alignment.centerRight,
          child: ElevatedButton(
            onPressed: () => onNext(),
            style: ButtonStyle(
              shape: MaterialStateProperty.all(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
              backgroundColor: MaterialStateProperty.all(Color(0xFF50BE91)),
              padding: MaterialStateProperty.all(EdgeInsets.symmetric(horizontal: 30.0)),
            ),
            child: Text('Next'),
          ),
        ),
      ],
    );
  }
}

class PasswordPage extends StatelessWidget {
  final VoidCallback onNext;

  const PasswordPage({Key key, @required this.onNext}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Create a password \nfor your account.',
              style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('3/3'),
          ],
        ),
        const SizedBox(
          height: 20.0,
        ),
        Text('Create password'),
        const SizedBox(height: 10.0),
        TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
          ),
        ),
        const SizedBox(
          height: 10.0,
        ),
        Text(
          'Should be 8 character long.\nContains symbol and one number',
          style: TextStyle(
            color: Colors.black.withOpacity(0.5),
          ),
        ),
        const SizedBox(height: 20.0),
        Text('Create password'),
        const SizedBox(height: 10.0),
        TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
          ),
        ),
        const SizedBox(
          height: 30.0,
        ),
        Align(
          alignment: Alignment.center,
          child: RoundedButton(
            backgroundColor: const Color(0xFF50BE91),
            textColor: Colors.white,
            text: 'Create account',
            onPressed: () {
              _scaffoldKey.currentState.showSnackBar(
                SnackBar(
                  margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                  behavior: SnackBarBehavior.floating,
                  content: Text('Signed in as shyamad280@gmail.com'),
                  action: SnackBarAction(
                    label: 'Done',
                    textColor: Color(0xFF50BE91),
                    onPressed: () {},
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
