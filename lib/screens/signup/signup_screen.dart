import 'package:flutter/material.dart';
import 'package:flutter_assignment/screens/login/login_screen.dart';
import 'package:flutter_assignment/screens/signup/sign_up_on_board_screen.dart';
import 'package:flutter_assignment/widgets/rounded_button.dart';

class SignupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text('Sign up'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'test app',
              style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 20.0),
            Text(
              'Create an\naccount.',
              style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10.0),
            Text(
              'Track share collaborate for your child with special needs',
              style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 18.0,
                height: 1.6,
              ),
            ),
            const SizedBox(height: 10.0),
            Spacer(),
            Align(
              alignment: Alignment.center,
              child: RoundedButton(
                backgroundColor: const Color(0xFF50BE91),
                textColor: Colors.white,
                text: 'Lets start',
                onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => SignUpOnBoardScreen())),
              ),
            ),
            Spacer(),
            GestureDetector(
              onTap: () => Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => LoginScreen())),
              child: Container(
                alignment: Alignment.center,
                color: Color(0xFFF5F5F5),
                height: 50.0,
                child: RichText(
                  text: TextSpan(
                    text: 'Already have an account?',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Inter',
                      color: Colors.black,
                    ),
                    children: [
                      TextSpan(
                        text: ' log in',
                        style: TextStyle(
                          fontSize: 16.0,
                          fontFamily: 'Inter',
                          color: Color(0xFF50BE91),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
