import 'package:flutter/material.dart';
import 'package:flutter_assignment/widgets/rounded_button.dart';

class ForgotPasswordScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final scaffoldKey = GlobalKey<ScaffoldState>();
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text('Forgot password'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Reset your password',
              style: TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10.0),
            Text(
              'We will send you and email with the password reset link which will redirect you page.',
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
              ),
            ),
            const SizedBox(height: 20.0),
            Text('Email address'),
            const SizedBox(height: 10.0),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
              ),
            ),
            const SizedBox(height: 20.0),
            Align(
              alignment: Alignment.center,
              child: RoundedButton(
                backgroundColor: const Color(0xFF50BE91),
                textColor: Colors.white,
                text: 'Send link',
                onPressed: () {
                  scaffoldKey.currentState.showSnackBar(
                    SnackBar(
                      margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                      behavior: SnackBarBehavior.floating,
                      content: Text('Sent to shyamad280@gmail.com'),
                      action: SnackBarAction(
                        label: 'Done',
                        textColor: Color(0xFF50BE91),
                        onPressed: () {},
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
