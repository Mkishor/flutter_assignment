import 'package:flutter/material.dart';
import 'package:flutter_assignment/screens/login/forgot_password_screen.dart';
import 'package:flutter_assignment/screens/signup/signup_screen.dart';
import 'package:flutter_assignment/widgets/rounded_button.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text('Log in'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(height: 20.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              'Login with\nyour email.',
              style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 20.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text('Email address'),
          ),
          const SizedBox(height: 10.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
              ),
            ),
          ),
          const SizedBox(height: 20.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text('Password'),
          ),
          const SizedBox(height: 10.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
              ),
            ),
          ),
          const SizedBox(height: 20.0),
          TextButton(
            onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => ForgotPasswordScreen())),
            child: Text(
              'Forgot password?',
              style: TextStyle(
                fontSize: 16.0,
                color: Color(0xFF50BE91),
              ),
            ),
          ),
          const SizedBox(height: 20.0),
          Align(
            alignment: Alignment.center,
            child: RoundedButton(
              backgroundColor: const Color(0xFF50BE91),
              textColor: Colors.white,
              text: 'Login',
              onPressed: () {},
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: () => Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => SignupScreen())),
            child: Container(
              alignment: Alignment.center,
              color: Color(0xFFF5F5F5),
              height: 50.0,
              child: RichText(
                text: TextSpan(
                  text: 'Dont have an account?',
                  style: TextStyle(
                    fontSize: 16.0,
                    fontFamily: 'Inter',
                    color: Colors.black,
                  ),
                  children: [
                    TextSpan(
                      text: ' sign up',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontFamily: 'Inter',
                        color: Color(0xFF50BE91),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
