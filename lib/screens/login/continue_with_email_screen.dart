import 'package:flutter/material.dart';
import 'package:flutter_assignment/screens/login/login_screen.dart';
import 'package:flutter_assignment/screens/signup/signup_screen.dart';

class ContinueWithEmailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text('Log in'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(height: 20.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              'test app',
              style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 15.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              'Good to see\nyou again.',
              style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 20.0),
          Divider(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: GestureDetector(
              onTap: () => Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => LoginScreen())),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Row(
                  children: [
                    Icon(
                      Icons.email,
                      color: Color(0xFF50BE91),
                    ),
                    const SizedBox(width: 10.0),
                    Text(
                      'Continue with email',
                      style: TextStyle(fontSize: 16.0),
                    )
                  ],
                ),
              ),
            ),
          ),
          Divider(),
          Spacer(),
          GestureDetector(
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => SignupScreen())),
            child: Container(
              alignment: Alignment.center,
              color: Color(0xFFF5F5F5),
              height: 50.0,
              child: RichText(
                text: TextSpan(
                  text: 'Dont have an account?',
                  style: TextStyle(
                    fontSize: 16.0,
                    fontFamily: 'Inter',
                    color: Colors.black,
                  ),
                  children: [
                    TextSpan(
                      text: ' sign up',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontFamily: 'Inter',
                        color: Color(0xFF50BE91),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
