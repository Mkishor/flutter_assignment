import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final VoidCallback onPressed;
  final Color backgroundColor;
  final Color textColor;
  final String text;
  final Border border;

  const RoundedButton({
    Key key,
    @required this.onPressed,
    @required this.backgroundColor,
    @required this.textColor,
    @required this.text,
    this.border = const Border(),
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Material(
      color: backgroundColor,
      borderRadius: BorderRadius.circular(25.0),
      child: InkWell(
        splashColor: Colors.white.withOpacity(0.3),
        onTap: onPressed,
        child: Container(
          height: 45.0,
          width: size.width * .60,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25.0),
            border: border,
          ),
          child: Text(
            text,
            style: TextStyle(
              fontFamily: 'Inter',
              fontSize: 18.0,
              fontWeight: FontWeight.w500,
              color: textColor,
            ),
          ),
        ),
      ),
    );
  }
}
